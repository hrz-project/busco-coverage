---
title: Estimation of Coverage on BUSCO
author: samuel barreto
date: 2020-05-26T11:51:43+0200
---

# BUSCO Coverage

Estimation of coverage on BUSCOs.

## Motivation

Genome size is a useful indicator for postulating *a priori* the sequencing efforts required.
One common strategy is to sequence libraries to a given amount (say produce 500 M reads per genome).
Then genome size (_C_) can be estimated using various approaches.
Knowing _C_ is a useful way to deduce the remaining sequencing efforts.

_C_ is commonly inferred using k-mer frequency-based approaches, as implemented in [`findGSE`](https://github.com/schneebergerlab/findGSE) (Sun et al, 2018).
However, k-mer frequency based approaches to genome size estimation can not operate with really low coverage data, the crux of their algorithm relying on discriminating low-frequency k-mer (errors) from high-frequency ones (haplo- or diploid sequences).
When coverage is low, noise and signal are conflated.

This software proposes to use the read coverage measured on genes identified as being universal single-copy orthologs, as used by the commonly used [`BUSCO`](https://busco.ezlab.org/) pipeline (Seppey et al, 2019).

## Description

`busco-coverage` takes as input an assembled genome, the sequencing reads that were used to assemble this genome, and the path to the OrthoDB `ancestral` file containing consensus protein sequences used by `BUSCO`.
(These can be downloaded from [`BUSCO` website](https://busco.ezlab.org/busco_v4_data.html).)
It produces as output the per-position read coverage in all regions with sufficient homology to a BUSCO protein sequence.

## Installation

```bash
# install to /usr/local/bin/busco-coverage
make install
# install to ~/.local/bin/busco-coverage
PREFIX=~/.local/ make install
```

## Usage

```bash
$ busco-coverage --help
```

```
Usage: busco-coverage \
            --in <r_{1,2}.fq.gz> \
            --busco <ancestral> \
            --reference <assembly.fasta> \
            --outdir <dir> \
            --prefix <r>

[...]

Options:

  -i,--in X_{1,2}.fastq.gz       Sequencing reads of sample
  -b,--busco                     busco orthologs database
  -r,--reference                 reference assembly (fasta)
  -o,--outdir <busco-coverage>   output directory
  -p,--prefix <X>                prefix of output files

  -c,--cpus <8>                  Number of allocated cpus
  -l,--log <<prefix>_err.log>    Log file
  -v,--verbose,--no-verbose      Toggle verbose mode on/off
  -n,--dry-run,--no-dry-run      Toggle dry-run mode on/off
  -h,--help                      Display this message.
```

## Dependencies

- [`perl`](https://www.perl.org/), >= v5.24
- [`bgzip`](https://www.htslib.org/doc/bgzip.html),
- [`samtools`](https://www.htslib.org/), >= 1.10. Older versions won’t
  work, as support for excluding duplicates in computing coverage was
  added in 1.10
- [`bwa-mem2`](https://github.com/bwa-mem2/bwa-mem2), 2.0pre2
- [`GNU Parallel`](https://www.gnu.org/software/parallel/), 20200322 (O. Tange 2018).
- [`diamond`](https://github.com/bbuchfink/diamond), 0.9.32.133
- [`bedtools`](https://bedtools.readthedocs.io/en/latest/),
- [`R`](https://www.r-project.org/), >= 3.6
- [`data.table`](https://rdatatable.gitlab.io/data.table/index.html), >= 1.12.9
- [`seqkit`](https://bioinf.shenwei.me/seqkit/), 0.12.1
- [`samtools-dedup`](FIXME), 0.1.0


## Methods

The homology is detected using `diamond blastx` (Buchfink et al, 2015);
read mapping is performed using `bwa-mem2`;
sequence cleaning by `seqkit`;
`diamond blastx` output parsing by `R` and the `data.table` package,
read coverage is estimated using `samtools`.

![Workflow](doc/workflow.png)

## References

- O. Tange (2018): GNU Parallel 2018, Mar 2018, ISBN 9781387509881, DOI https://doi.org/10.5281/zenodo.1146014
- Hequan Sun et al., “FindGSE: Estimating Genome Size Variation within Human and Arabidopsis Using k-Mer Frequencies,” Bioinformatics 34, no. 4 (February 15, 2018): 550–57, https://doi.org/10.1093/bioinformatics/btx637.
- Mathieu Seppey, Mosè Manni, and Evgeny M. Zdobnov, “BUSCO: Assessing Genome Assembly and Annotation Completeness,” in Gene Prediction: Methods and Protocols, ed. Martin Kollmar, Methods in Molecular Biology (New York, NY: Springer, 2019), 227–45, https://doi.org/10.1007/978-1-4939-9173-0_14.
- Benjamin Buchfink, Chao Xie, and Daniel H. Huson, “Fast and Sensitive Protein Alignment Using DIAMOND,” Nature Methods 12, no. 1 (January 2015): 59–60, https://doi.org/10.1038/nmeth.3176.

# License

[gpl-v3](https://www.gnu.org/licenses/gpl-3.0.en.html)

> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.
>
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.
>
> You should have received a copy of the GNU General Public License
> along with this program.  If not, see <https://www.gnu.org/licenses/>.
