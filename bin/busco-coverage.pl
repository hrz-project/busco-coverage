#!/usr/bin/env perl

# busco-coverage
# authored by Samuel Barreto <samuel.barreto8@gmail.com>
# created at 2020-05-26T10:35:03+0200

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


use strict;
use warnings;
use v5.24;

use FindBin qw($Script);
use File::Basename qw(basename);
use File::Path qw(make_path);
use File::Temp qw(tempfile tempdir);
use Cwd qw(abs_path);
use POSIX qw(strftime);
use Getopt::Long qw(GetOptions);

use constant DEBUG => $ENV{'DEBUG'};
use constant EXE   => File::Basename::basename(__FILE__);

my @arg_in;
my $arg_outdir;
my $arg_dryrun;
my $arg_help;
my $arg_verbose;
my $arg_cpus;
my $arg_log;
my $arg_memory;
my $arg_buscodb;
my $arg_prefix;
my $arg_reference;
my $arg_minlength;
my $arg_max_evalue;

usage(1) unless @ARGV;

GetOptions(
    "i|in=s{2}"      => \@arg_in,
    "b|busco=s"      => \$arg_buscodb,
    "o|outdir=s"     => \$arg_outdir,
    "r|reference=s"  => \$arg_reference,
    "L|minlength=i"  => \$arg_minlength,
    "e|max-evalue=f" => \$arg_max_evalue,
    "p|prefix=s"     => \$arg_prefix,
    "n|dryrun"       => \$arg_dryrun,
    "h|help"         => \$arg_help,
    "v|verbose!"     => \$arg_verbose,
    "c|cpus=i"       => \$arg_cpus,
    "l|log=s"        => \$arg_log,
) or usage(1);
usage(1) if $arg_help;

$arg_reference or usage(2);
$arg_buscodb   or usage(2);
@arg_in        or usage(2);

#  ------------------------------------------------------- set defaults
$arg_minlength  ||= 0;
$arg_max_evalue ||= 1e-100;

$arg_prefix ||= get_prefix( $arg_in[0] );
$arg_outdir ||= "busco-coverage"; # set default arg outdir
mkp($arg_outdir);

my $prefix = "${arg_outdir}/${arg_prefix}";
$arg_log ||= "${prefix}_err.log";

$arg_cpus ||= 8;

#  --------------------------------------------------------------- main
my ( $R1, $R2 ) = @arg_in;

my $ref = cleanup_reference($arg_reference);
index_ref($ref);

my $buscodb    = diamond_db($arg_buscodb);
my $busco_blst = find_buscos( $ref, $buscodb );
my $busco_bed  = blast_to_bed($busco_blst);
$busco_bed  = bedtools_sort($busco_bed);

my $cram = map_reads( $ref, $R1, $R2 );
index_cram($cram);

my $cov = coverage( $cram, $ref, $busco_bed );
my $covgz = compress( $cov );


# removing temp file
# clean_up( $R1, $R2, $prefix );

#  -------------------------------------------------------------- utils
# sub clean_up {
#     my ( $R1, $R2, $prefix ) = @_;
#     my $scaffold_bubble = "${prefix}_scaffoldBubble.fa";
#     my $contig_bubble   = "${prefix}_contigBubble.fa";
#     my $scaffold_cpnt   = "${prefix}_scaffoldComponent.tsv";
#     my $kmerfreq        = "${prefix}_${arg_kmer}merFreq.tsv";
#     my $insfreq         = "${prefix}_lib1_insFreq.tsv";
#     msg( "Cleaning up ..." );
#     unless ($arg_dryrun) {
#         unlink( $R1, $R2, $scaffold_bubble, $contig_bubble, $scaffold_cpnt, $kmerfreq,
#                 $insfreq );
#     }
# }

sub compress {
    my $file = shift;
    my $output = "${file}.gz";
    -f $output or run_cmd("bgzip -@ $arg_cpus $file");
    return $output;
}

sub coverage {
    my ( $cram, $ref, $bed ) = @_;
    my $lobed = split_bed($bed);
    # 1024 is flag for DUP
    my $fcmd = "samtools depth -G 1024 -a -b {} --reference $ref $cram";
    my $output = "${prefix}.busco-coverage.txt";
    my $cmd = "parallel -j $arg_cpus $fcmd :::: $lobed | sort -k1 > $output";
    -f $output or run_cmd($cmd);
    rm_split_bed($lobed);
    return $output;
}

sub markdup {
    my ( $cram, $ref ) = @_;
}

sub rm_split_bed {
    my $bedlof = shift;
    open(my $fh, '<:encoding(UTF-8)', $bedlof)
      or die "Could not open file '$bedlof' $!";
    while (<$fh>)
      {
          chomp;
          unlink $_;
      }
    close $fh;
}

sub split_bed {
    my ($bed) = @_;
    my @splitted = get_cmd("split --verbose --additional-suffix .bed -n l/$arg_cpus $bed ${prefix}.");
    my ($fh, $lobed) = tempfile();
    foreach my $splt (@splitted)
      {
          chomp $splt;
          ( my $fn = $splt ) =~ s/creating file '(.*)'/$1/;
          print $fh "$fn\n";
      }
    return $lobed;
}

sub bedtools_sort {
    my ($bed) = @_;
    my $output = "${prefix}.busco-hsp.bed";
    -f $output or run_cmd("bedtools sort -i $bed > $output");
    return $output;
}

sub blast_to_bed {
    my ($blast) = @_;
    my $output = "${prefix}.busco-blast.bed";
    my $script = <<EOF;
library(data.table)
args <- commandArgs(TRUE)

d <- fread(args[[1]])
colnames(d) <- c("query_id", "subject_id", "identity",
                 "length", "mismatches", "gap",
                 "q_start", "q_end", "s_start", "s_end", "evalue",
                 "bits" )

d <- d[, .(query_id, q_start, q_end, evalue)]
d[q_start > q_end, c("q_start", "q_end") := .(q_end, q_start)]
d[, q_end := q_end - 1]
d[, length := q_end - q_start]
## remove shitty evalue
d <- d[evalue < quantile(evalue, 0.95)]
## chose longest hit per busco
d <- d[order(-length)][, .SD[1], by = query_id][order(query_id)]
## format for bed output
d <- d[, .(query_id, q_start, q_end)]
fwrite(d, args[[2]], sep = "\\t", col.names = FALSE)
EOF
    my ($fh, $scriptname) = tempfile();
    print $fh $script;
    -f $output or run_cmd("Rscript $scriptname $blast $output");
    return $output;
}

sub find_buscos {
    my ( $reference, $buscos ) = @_;
    my $output = "${prefix}.busco.blast";
    my $cmd    = "diamond blastx --db $prefix --query $reference 2> /dev/null "
      . " > $output";
    -f $output or run_cmd($cmd);
    return $output;
}

sub index_cram {
    my ($cram) = @_;
    -f "$cram.crai" or run_cmd("samtools index -@ $arg_cpus $cram");
}

sub map_reads {
    my ( $ref, $R1, $R2 ) = @_;
    # temporary cram file with no duplicates marked.
    my $cram = "${prefix}.tmp.cram";
    # cram file with duplicates marked.
    my $output = "${prefix}.cram";
    my $bwa_cmd =
      "bwa-mem2 mem -t $arg_cpus $ref $R1 $R2 2>> $arg_log "
      . "| samtools sort -@ $arg_cpus --reference $ref - 2>> ${arg_log} "
      . "| samtools view -@ $arg_cpus -C -T $ref -o $cram -";
    my $dup_cmd =
      "samtools-dedup -c $arg_cpus -i $cram -o $output";
    unless (-f $output) {
        -f $cram or run_cmd($bwa_cmd);
        run_cmd($dup_cmd);
    }
    return $output;
}

sub index_ref {
    my ($reference) = @_;
    unless (-f "$reference.fai") {
        run_cmd("bwa-mem2 index $reference 2>> ${arg_log}");
        run_cmd("samtools faidx $reference 2>> ${arg_log}");
    };
}

sub diamond_db {
    my ($busco) = @_;
    my $outdb = "${prefix}.dmnd";
    -f $outdb or run_cmd("diamond makedb --db $prefix --in $busco 2>> $arg_log");
    return $outdb;
}

sub cleanup_reference {
    my ($ref) = @_;
    my $outref = "${prefix}.ref.fa";
    -f $outref or clean_ref_headers($ref,$outref);
    return $outref;
}

sub clean_ref_headers {
    my ($ref,$cleanref) = @_;
    open(my $fh_input, '<:encoding(UTF-8)', $ref) or die "Could not open file: $!";
    open(my $fh_output, '>:encoding(UTF-8)', $cleanref) or die "Could not open file: $!";
    open(my $fh_table_output, '>:encoding(UTF-8)', "${prefix}.headers.txt") or die "Could not open file: $!";
    my $counter = 1;
    while (<$fh_input>) {
        if ($_ =~ /^>/) {
            chomp;
            print $fh_output ">NODE_$counter\n";
            print $fh_table_output "$_\t$counter\n";
            $counter += 1;
        } else {
            print $fh_output $_;
        }
    }
    close $fh_input;
    close $fh_output;
    close $fh_table_output;
    return $cleanref;
}

sub get_prefix {
    my ($x) = @_;
    $x =~ s/(^.*)_1\..*\.gz/$1/;
    return basename($x);
}

sub msg {
    my $time = strftime "%H:%M:%S", localtime;
    my $EXE  = EXE;
    my $msg  = "# [$EXE] ($time): @_\n";
    print STDERR $msg;
}

sub err { msg(@_) && exit(1); }

sub dbg { msg(@_) if DEBUG; }

sub get_cmd {
    my ($cmd) = @_;
    ( my $msg = $cmd ) =~ s/[\n\r\\]//g;
    dbg("Running: '$msg'");
    return `$cmd`;
}

sub run_cmd {
    my ($cmd) = @_;
    dbg("Running: '$cmd'");
    my $er = 0;
    unless ($arg_dryrun) {
        $er = system( "bash", "-c", $cmd );
    }
    $er == 0 or err ("Error $? running command: $!");
    return 1 unless $er;
}

sub mkp { make_path(@_) unless $arg_dryrun; }

#  -------------------------------------------------------------- USAGE

sub usage {
    my ($exitcode) = @_;
    my $EXE        = EXE;
    my $usage      = <<EOF;
Usage: $EXE \\
            --in <r_{1,2}.fq.gz> \\
            --busco <ancestral> \\
            --reference <assembly.fasta> \\
            --outdir <dir> \\
            --prefix <r>

Estimate read coverage of universal single copy orthologs, using read
mapping.

Options:

  -i,--in X_{1,2}.fastq.gz       Sequencing reads of sample
  -b,--busco                     busco orthologs database
  -r,--reference                 reference assembly (fasta)
  -o,--outdir <busco-coverage>   output directory
  -p,--prefix <X>                prefix of output files

  -c,--cpus <8>                  Number of allocated cpus
  -l,--log <<prefix>_err.log>    Log file
  -v,--verbose,--no-verbose      Toggle verbose mode on/off
  -n,--dry-run,--no-dry-run      Toggle dry-run mode on/off
  -h,--help                      Display this message.
EOF

    print STDERR $usage;
    exit($exitcode);
}

__END__
