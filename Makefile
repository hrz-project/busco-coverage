# am_BUSCO-Coverage_2020-05-26
# created at <2020-05-26T10:35:03+0200>
# authored by: <Samuel Barreto>

SHELL=bash

ifndef PREFIX
PREFIX := /usr/local
endif

.PHONY: install
install:
	ln -s $(PWD)/bin/busco-coverage.pl $(PREFIX)/bin/busco-coverage

# DOCUMENTATION ------------------------------------------------------

.PHONY: doc
doc: doc/manual.pdf

doc/manual.pdf: doc/workflow.png doc/gpl-logo.png
doc/gpl-logo.png:
	wget -O $@ "https://www.gnu.org/graphics/gplv3-88x31.png"

doc/manual.pdf: README.md
	pandoc -i $< -o $@ \
		--template eisvogel.latex \
		--listings \
		--from markdown+footnotes+pipe_tables+link_attributes \
		--filter pandoc-citeproc \
		--bibliography "$$HOME/Dropbox/bibliography/references.json" \
		--csl "$$HOME/Dropbox/bibliography/styles/chicago-author-date.csl"

doc/%.png: doc/%.dot ; dot -Tpng -Gdpi=300 $< > $@
